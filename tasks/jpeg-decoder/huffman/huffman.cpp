#include "huffman.h"

void HuffmanTree::Build(const std::vector<uint8_t> &code_lengths,
                        const std::vector<uint8_t> &values) {
    (void)code_lengths;
    (void)values;
}

bool HuffmanTree::Move(bool bit, int &value) {
    (void)bit;
    (void)value;

    return true;
}
